README - python rewrite of parseCPP and pack2sim - by Noah Wuerfel
(nwuerfel@college.harvard.edu)

All useful code currently resides in parseAndProcess.py in the 'noah' folder of
the 'pythonrewrite' directory

This is the python rewrite of the parseCPP and pack2sim pipelines originally
implemented in Matlab to analyze the output of a C-language simulation and
prepare data packets suitable for hardware input. The goal of the rewrite is
twofold. First, because a python script is later used to compare software and
hardware results, the rewrite provides a language unified infrastructure for
hardware design and debugging. Second, the matlab implementation proved
difficult to read and use for newcomers and a python rewrite seemed appropriate
in creating a user friendly software package.

A NOTE ON THE STRUCTURE OF THE CODE:
------------------------------------
For now, the python rewrite is simply a cleaned up port of the original matlab
code into python. The same workflow and logic is maintained for granularity and
the familiarity of those currently using the codebase. However, there's many
ways to improve the speed and efficiency of parseAndProcess to more fully
utilize tools unique to python/numpy. Numpy arrays and python lists each capture
some of the functionality of matlab arrays. To keep the data layout as identicle
to the original matlab as possible, a mix of numpy arrays and python lists are
used to process the data. The motivation behind this is to maintain the data
format currently familiar to those using the codebase. In the future, changing
the workflow of the code to more properly leverage the structure of numpy arrays
could significantly simplify and speed up the code. This requires a reworking of
the logic flow of the pipeline- flattening data througout- but would be well
worth the effort. For now, it's nice to have a functional rework that mirrors
the original code. 

BASE HIT DATA SOURCE:
--------------------
Base data is currently stored in the file
mmt_hit_print_h0.0009_ctx2_ctuv1_uverr0.0035_setxxuvuvxx_ql1_qdlm1_qbg0_qt0_NOM_NOMPt100Gev.digi.ntuple.txt
which is the output of the C-language simulation. This file also contains event
summaries which are the output of the software trigger processor simulation.
Originally, hit data and software summaries were stored in the matlab arrays
Hits and Event_Fit_Info, respectively. In the python rewrite, parseData() saves
the event fit information as a numpy array in the file event_Fit_Info.txt. Hit
data is directly fed into the pack2sim pipeline when running main(). If desired,
parseData() can be run on its own as it returns a numpy array containing the Hit
information. This information can then be stored locally if desired by a user to
be loaded without reparsing the .txt. If useful, the code can easily be modified
to automatically store hitData and parse only when there is no extant hitData.

RUNNING THE CODE:
-----------------
To run the entirety of the code- parsing data and processing it into hardware
inputs- one need only launch python, import parseAndProcess, and run
parseAndProcess.main(). main() is a wrapper function that calls all necessary
functions in the pipeline. An optional argument 'loud=True' may be passed into
main to generate detailed output of event#, vmmid, and ART data from
flatPacketToBinary in the processing part of the code. Because python allows
helper functions to be called from the python command line, individual functions
can also be run to process data at each step. For more information on what
helper functions are available/what they do, check the function list at the
bottom of this document and comment headers for each function in the code file
itself.

HELP, MAIN FAILS BECAUSE IT CANNOT FIND DATA TO PARSE:
------------------------------------------------------
The main() function first calls parseData() to read in the hitData. The default
location for the data file is hardwritten at the top of the parseAndProcess.py
file. If a different data file is desired, or the data file lives in a different
place, a user must change this variable to point to the input file.

GBT PACKETS AND DATA FORMAT AT EACH STEP OF CODE:
-------------------------------------------------
A major source of confusion in the matlab code is the unclear labeling of
"packets". Many different data formats are refered to as a "packet" which causes
confusion in discussing and understanding the code. Following is the structure
and name of the data at different points in the pipeline as well as which
functions the data structure can be found in:

Data from the .txt file is of the form [time, vmm, plane, station, strip, slope]
parseData reads in this data format and restructures it to generate data called
"Hits" which is of the form [eventnum, bxid, plane, station, strip, slope]

Hits are passed into assemblePackets(). Plane, station, and strip information are
converted by the helper function extractLVA into layer, vmmid, and artdata
information. "Hits" are then restructured as "formattedHits" which have the form
[bxid, layer, plane, vmmid, art]

formattedHits are passed into the function constructPacketGuts() which generates
"packets" of the form [[layer, side], [packetData]]. formattedHits are fed into
the function buildPacketData to generate "packetData"
"packetData" takes the form [numHits, bxid, 8xVMMIDs, dataParity, 8xARTDatas],
which has 19 entries.

"dataParity" is an 8 bit integer value. For each event, up to 8 hits are chosen
by the function choosePreferencedHits(). Each of these hits has a corresponding
vmmid and artData value (which strip was hit in the vmmid). artData is a 6 bit
value representing which of the 64 strips in the vmm was hit. For each artData
value, a partiy is calculated: 0 for an even number of bits, 1 for an odd number
of bits. For the (up to) 8 artData values selected, the parity bit is calculated
for each. Then, these partiy bits are strung together to form the 8 bit integer
called "dataParity" which for now is represented in decimal, but later converted
back into an 8 bit binary string for the final data packet. I found a bug in the
original matlab code which has now been corrected where artData parity was
caluculated when the artData is analyzed, but later artData values are sorted
and rearranged and their parity bits would not get changed around to match them.
This meant that the artData parity value was completely wrong by the end of the
pack2sim pipeline.

The outputs of assemblePackets are also called "assembledPackets" in the code
and take the form [bxid, packet] which fully expands to:
[bxid,[[layer, side], [numHits, bxid, 8xVMMIDs, dataParity,8xARTDatas]]]
as one can see, this is quite a messy structure and bxid is unnecessarily
repeated. This is, I think, a very good place for improvement in the flattening
of data and streamlining of the code.

assembledPackets are then passed through the flattenPackets function which
outputs "flatPackets" of the form [bxid, layer, side, packet]. Again, this seems
like a redundant step, and the next update of this code ought to return packets
of this format straight out of assemblePackets. 

flatPackets are passed into pack2sim which passes the "packet" component of our
flatPackets into the function flatPacketToBinary. Again, this seems like an
inefficient use of our data structures and provides ample opportunity for
improvement. flatPacketToBinary does a great bit of work in reformatting
packetData into three different ouputs: addcFormat, pgFormat0, and pgFormat1

Each of these output formats is a binary representation of the data. addcFormat
represents what output from the addc would look like. the pattern generator
formats each represent what output from side 0 and side 1 fibers would look like
directly from the vmms. The exact layout of these formats is in
flatPacketsToBinary and its a little bit of a headache to look at all the bits.

pack2sim takes the binary representation of the packetData produced by
flatPacketsToBinary and converts it to a hex string representation of the data.
This is done via the helper function binStr2hexStr which also keeps/adds leading
zeros as needed. The data takes the form of some number of hex digits, a space,
then layer data. This hex formatted data is the final GBT packet structure and
is written to an outfile for input into the hardware. This outfile can be
specified in the main function, but the default for the addcFormat is
packetDataIn_v1.txt, while the pattern generator outfiles are always the
addcFormat outfile with a pg0 or pg1 prefix attatched. Something to note about
the bxids at this stage: since there's only 12 bits (3 hex digits) to contain
the bxid, at this point they are reported modulo 4096. 

A LIST OF FUNCTIONS:
--------------------
The following is a full list of the functions in parseAndProcess, all of which
can be run independantly in the python command line. I highly recommend the use
of exuberent ctags to expedite function references, but since they're all in the
same file you should be able to get away with normal searches inside the
document itself. To use exuberent ctags with vim, run the command 'ctags -R .'
in the 'pythonrewrite' parent directory to generate a tags file. Then, anywhere
in that directory or a subdirectory, one can immediately view any function in
vim by running the command 'vi -t <functionName>'. This acts as a fuzzy search
so even if you're off a bit in the spelling/name of function it should be able
to find it for you. Note, anytime a function is added to the code, ctags should
be run again to regenerate the tags file.

I have indented helper functions under the function that calls them, if a
function is called by multiple other functions, I only list it under one.

The List:
main(loud=false){
	parseData(inputfile=input_filename, outputfile=output_filename){
	}
	assemblePackets(Hits){
		extractLVA(plane, station, strip)	
		constructPacketGuts(formattedHits, Bxid)
			buildPacketData(formattedHits, side)
				choosePreferencedHits(formattedHits, side)
				computeParity(x)
					parity(x)
						makebinary(x)
					bin2dec(x)
	}
	flattenPackets(Packets){
	}
	pack2sim(flatpackets, outfile='packetDataIn_v1.txt', loud=false){
		flatPacketToBinary(packetData, side, loud=false)	
			dec2bin(x,nbits)
		binStr2hexStr(binStrArray)
		writeOut(outfile, data)
	}
}

findEventsInRegion(region, numEvents='')

COMPARING SW AND FW RESULTS:
----------------------------
The purpose of preparing gbt packets is to run simulations of the Trigger
Processor firmware with simulated data as input. Running TP simulations
generates a simulated firmware output file. This firmware output file is
compared to a software output file which is parsed out of Stephen's original
.txt file containing the Athena simulation data. 

This comparison, as well as analysis and graphing, are handled by Alex Tuna's
python and ROOT code. The structure of my repo has a directory called alex in
which I keep Alex's code. However, Alex's code is in his own repo so one must
download both repositories to run the whole process. The parseAndProcess suite
of code, then, deals primarily with the generation of gbt packets for simulation
while Alex's code deals with analysis of the simulation output.

The workflow for comparing sw and fw results, starting with nothing but
Stephen's .txt file containing Athena data is the following:
First, the parseAndProcess suite is run to generate GBT packets for simulation.
Next, GBT packets are run through the simulation, using hitTrack.do to point to
the generated packet file. I currently store the simulation results in
pythonrewrite/data/simResults, but they can go wherever is easy for you. Next,
we use Alex's parse_cpp.py script to parse data from Stephen's file into root.
It is important that the same events/ region are specified when generating GBT
packets and when using Alex's code to ensure that bxids match between the two.
Next, we use Alex's parse_fw_results.py script, passing in our simResults as
input, to parse simulation output into root. Finally, Alex's compare_fw_sw.py
script takes care of comparing the results as well as drawing nice graphs.

AN EXAMPLE FROM START TO FINISH:
--------------------------------
Here is an example in which we will generate GBT packets from 10 events in
region 6, run them through simulation, and then use Alex's code to compare fw
and sw results:

First, from the shell, we'll launch the python interpreter in the
pythonrewrite/noah directory:

[]$ python
>>> import parseAndProcess as pap
>>> events = pap.findEventsInRegion(6, numEvents='10')
>>> hits = pap.parseData(desiredEvents=events)
>>> assembledPackets = pap.assemblePackets(hits)
>>> flatPackets = pap.flattenPackets(assembledPackets)
>>> pap.pack2sim(flatPackets, outfile='packetDataIn_10EventsRegion6.txt')

Now, switching over to simulation, we point hitTrack.do to the newly generated
outfile: packetDataIn_10EventsRegion6.txt and run the simulation in questasim

Now, switch to the directory containing Alex's code: for me this is
pythonrewrite/alex/MMTP_scratch


CURRENTLY PARSE CPP HAS A MAGIC NUMBER TO CHOOSE NUMBER EVENTS IN DESIRED REGION
BUT THIS WILL EVENTUALLY BE REPLACED BY AN OPTION OR SOMETHING BETTER. Currently
it takes the first 10 events in a region.  

In the shell run:
[MMTP_scratch]$ python parse_cpp.py --input='path/to/stephen/data' --region=6
[MMTP_scratch]$ python parse_fw_results.py --input'/path/to/sim/results'
[MMTP_scratch]$ python compare_sw_fw.py

And we're done!

There's a ton in the code that can be cleaned up and made much much easier, like
making the whole parseAndProcess suite wrapped in a nice main function (this
works but not for specified regions yet). If you have any questions about the
operation of code, algorithms, or setting up feel free to email me.
