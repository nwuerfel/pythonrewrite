input_filename = "../data/mmt_hit_print_h0.0009_ctx2_ctuv1_uverr0.0035_setxxuvuvxx_ql1_qdlm1_qbg0_qt0_NOM_NOMPt100GeV.digi.ntuple.txt"   
lines = open(input_filename).readlines()

nzregions = 8
pitch = 0.445
startingBCID = 20
BCIDinterval = 5
offsetBottom = [65, 65, 73, 73, 73, 73, 65, 65]
offsetTop = [68, 68, 110, 110, 110, 110, 68, 68]


def bases(lines, coord, position):
	if coord not in ["Y", "Z"]:
		fatal("Bad coordinate for bases: %s" % coord)	
	if coord ==
