import numpy as np
import time
import sys

input_filename = "../data/stephen/mmt_hit_print_h0.0009_ctx2_ctuv1_uverr0.0035_setxxuvuvxx_ql1_qdlm1_qbg0_qt0_NOM_NOMPt100GeV.digi.ntuple.txt"   
sum_output_filename = "../data/eventSummary/event_Fit_Info.txt"
outfile_default = "packetDataIn_v1.txt"

# global wrapper function, runs entire pipeline to parse and process data
# the loud variable can be set if detailed information about vmmids and ART data
# is desired during the pack2sim part of the pipeline (specificially in
# flatpacket to binary)
def main(loud=False, timeit=False, outfile=outfile_default, desiredEvents=[]):

    print 'Parsing Data...'

    if(timeit):
        t0 = time.clock()
    data = parseData(desiredEvents=desiredEvents)

    if(timeit):
        t1 = time.clock()
        print 'Parsing time: %4f seconds' % (t1-t0)

    print 'Parsing complete, assembling packets...'
    packets = assemblePackets(data)

    if(timeit):
        t2 = time.clock()
        print 'Assembling time: %4f seconds' % (t2-t1)

    print 'Flattening packets...'
    flatpackets = flattenPackets(packets)

    if(timeit):
        t3 = time.clock()
        print 'Flattening time: %4f seconds' % (t3-t2)

    print 'Converting flatpackets into simData...'
    [simlen, pg0len, pg1len, addcFormat] = pack2sim(flatpackets, outfile, loud=loud)

    if(timeit):
        t4 = time.clock()
        print 'pack2sim time: %4f seconds' % (t4-t3)

    print 'wrote %i lines of simData' % simlen
    print 'wrote %i lines of pg0Data' % pg0len
    print 'wrote %i lines of pg1Data' % pg1len

    if(timeit):
        print 'Total time elapsed: %6f' % (t4-t0)

    print 'Done!'

# pack2sim takes flatpackets and generates hex packets to send out to hardware a
# great deal of the work is done in flatPacketToBinary which actually processes
# the packet information. The rest of pack2sim is reformatting the output of
# flatPacketToBinary into hex
def pack2sim(flatpackets, outfile='', loud=False):
    # outfile constants
    pgoutfile0 = '../data/simData/'+'pg0' + outfile
    pgoutfile1 = '../data/simData/'+'pg1' + outfile
    outfile = '../data/simData/'+outfile

    # generate binary representation of data
    [addcFormat, pgFormat0, pgFormat1] = flatPacketToBinary(flatpackets[:,3:], flatpackets[:,2], loud)
    
    # convert binary representation into hex representation
    addcFormatHex = binStr2hexStr(addcFormat)
    pgFormatHex0 = binStr2hexStr(pgFormat0)
    pgFormatHex1 = binStr2hexStr(pgFormat1)

    # build the addcHexFormat
    addcFormatHexChunks = []
    for index, word in enumerate(addcFormatHex):
        newWord = ''.join(('0000',word))
        wordlen = len(newWord)
        wordParts = [newWord[i:i+8] for i in range(0, wordlen, 8)]
        for part in wordParts:
            addcFormatHexChunks.append(part)

    # build layer info to add to the addcHex info
    layerInfo = []
    for packet in flatpackets:
        fiberInfo = packet[1]*2 + packet[2] + 32
        for i in range(0,4):
            layerInfo.append(hex(fiberInfo).rstrip('L').lstrip('0x'))
    
    # build simData (addcFormat that gets input into the hardware)
    simData = []
    for index, word in enumerate(addcFormatHexChunks):
        simData.append(word+' '+layerInfo[index]+'\n')

    # build pattern generator sim data for side 0
    pgSimData0 = []
    for line in pgFormatHex0:
        pgSimData0.append(line[0:8]+' '+line[8:]+'\n')

    # build pattern generator sim data for side 1
    pgSimData1 = []
    for line in pgFormatHex1:
        pgSimData1.append(line[0:8]+' '+line[8:]+'\n')
    
    # write out the data to various outfiles
    writeOut(outfile, simData)
    writeOut(pgoutfile0, pgSimData0)
    writeOut(pgoutfile1, pgSimData1)

    print 'wrote data to %s, %s, and %s' % (outfile, pgoutfile0, pgoutfile1)

    return [len(simData), len(pgSimData0), len(pgSimData1), addcFormat]

# writes every line in data array out to the chosen outfile
def writeOut(outfile, data):
    f = open(outfile, 'wb')
    for line in data:
        f.write(line)

# a helper function that takes a binary string and converts to a hex string
# suitable for the final output format. Importantly, this adds/preserves leading
# zeros as well as computes hex represenations of the binary input array
def binStr2hexStr(binStrArray):
    # need empty array to build up the hexstr
    hexStr = []
    hexLen = len(binStrArray[0])/4

    # convert every str in the binStrArray
    for binStr in binStrArray:
        # python adds L and 0x to hex strings but we don't want that in the data
        hexStr.append(hex(int(binStr,2)).rstrip('L').lstrip('0x').zfill(hexLen))

    return hexStr

# takes packetData from flatpackets and creates their binary representation
def flatPacketToBinary(packetData, side, loud=False):
    # constants
    numHits = packetData[:,0]
    bxid = packetData[:,1]
    vmmIDs = packetData[:,2:10] 
    artData = packetData[:,11:]

    # same shape as vmmID but marks the side of each vmm
    pgSide = np.copy(vmmIDs)
    for index, item in enumerate(pgSide):
        if side[index] == 1:
            item[item > 0] = 1
        else:
            item[item > 0] = 0

    # we'll fill out the hitmap later
    hitMap = np.zeros([packetData[:,0].size,32])

    # computes the binary representation of the bxid
    pgBxid = dec2bin(bxid, 26)
    tpBxid = dec2bin(bxid, 12)

    # empty pattern generator arrays to fill
    pgFormat1 = []
    pgFormat0 = []

    # build the pgFormat data   
    for packIndex, packet in enumerate(packetData):
        if loud:
            print 'event:%s' % format(bxid[packIndex], '03x')
        vmmRange = np.arange(numHits[packIndex])
        
        for index in vmmRange:
            localArt = artData[packIndex, index]
            localVmm = vmmIDs[packIndex, index]
            pgArtData = dec2bin([localArt], 6)
            pgVmmData = dec2bin([localVmm], 5)
            if loud:
                print "VMM: %s ART: %s" % (format(localVmm, '03x'), format(localArt, '03x'))
            pgEntry = str(pgBxid[packIndex][0]) + str(pgArtData[0][0]) + '000' + str(pgVmmData[0][0])
            if pgSide[packIndex, index] == 1:
                pgFormat1.append(pgEntry)
            else:
                pgFormat0.append(pgEntry)
        
        # mark all empty vmmIDs (places where there were less than 8 hits for an
        # event). We must be careful not to count '0' vmmid as a nonvalid hit
        vmmIDs[packIndex][numHits[packIndex]:] = 33 

        # we also sort the vmmid and artdata the order is filled then empty
        vmmOrder = np.argsort(vmmIDs[packIndex])    
        artData[packIndex] = artData[packIndex][vmmOrder]
        artData[packIndex] = artData[packIndex][::-1]
        
        # mark the hitmap in every place we have a valid vmmid
        for id in vmmIDs[packIndex]:
            if id != 33:
                hitMap[packIndex, id] = 1
        hitMap[packIndex] = hitMap[packIndex][::-1]

    # build the actual hitmap
    hitMapFinal = []
    for line in hitMap:
        newline = ''
        for word in line:
            newline = ''.join((newline, str(int(word))))
        hitMapFinal.append(newline)


    # TODO FIX EARLIER INSTANCES
    # NEED TO CALC THIS HERE BECAUSE WE NEED ORDER PRESERVED
    # EARLIER VERSIONS HAD BUG WHERE ARTDATA PARITY MATCHED ORIGNAL ORDER
    artDataParity = []
    for line in artData:
        artDataParity.append(dec2bin([computeParity(line)], 8))

    # build actual artData layout   
    artDataFinal = []
    for line in artData:
        newline = ''
        line = dec2bin(line, 6)
        for word in line:
            newline = ''.join((newline, word[0]))
        artDataFinal.append(newline)
    
    # here we build the addcFormat data
    addcFormat = []
    for index, line in enumerate(hitMap):
        addcFormat.append('1010'+tpBxid[index][0]+'00000000'+hitMapFinal[index]+artDataParity[index][0][0]+artDataFinal[index])

    return [addcFormat, pgFormat0, pgFormat1]   

# "flattens" packets by removing the nested list structure
# flat packets have the form [bxid, layer, side, packetData]
# where packetData is 19 ints of the form [numhits, bxid, 8xVmmIDs, artparity,
# 8xArtDatas] so the whole packet is 22 values longs
def flattenPackets(Packets):
    # extract data from assemblePacket output
    flatPackets = []
    for Packet in Packets:
        bxid = Packet[0][0]
        layer = Packet[1][0][0]
        side = Packet[1][0][1]
        packetData = Packet[1][1]
        flatPacketStub = [bxid, layer, side]
        
        # We don't want the packet data to be a nested list
        # We have to use extend to add the values without their list structure
        # We're getting rid of the listed structure -> flattening
        flatPacketStub.extend(packetData)
        flatPacket = flatPacketStub
        flatPackets.append(flatPacket)

    return np.asarray(flatPackets, dtype=int)

# takes Hits and generates assembledPackets which take the form
# [bxid,[packet]]
def assemblePackets(Hits):
    # constants
    bxidCol = 1
    planeCol = 2
    stationCol = 3
    stripCol = 4
    
    # extract useful stuff from the Hits
    bxid = Hits[:, bxidCol]
    plane = Hits[:, planeCol]
    station = Hits[:, stationCol]
    strip = Hits[:, stripCol]   
    [layer, vmmid, art] = extractLVA(plane, station, strip)

    # later fibers will be wired vertically on a plane
    # currently fibers are wired in Z across multiple planes
    # the result is that the layer is always 0
    layer = np.zeros(layer.shape)

    # some combination of me and python being stupid -> need to 
    # vertically concatenate then transpose to get desired shape
    formattedHits = np.vstack((bxid, layer, plane, vmmid, art))
    formattedHits = formattedHits.T 
    
    # construct packets for each unique bxid
    Bxids = np.unique(bxid)
    packetInitialized = False
    Packets = []
    for item in Bxids:
        thePacket = constructPacketGuts(formattedHits, item)
        if len(thePacket) != 0:
            for pkt in thePacket:
                Packets.append([[item], pkt])

    return Packets

# constructPacketGuts generates packets of the form
# [[layer, side],[packetData]] where
# packetData has the form [numHits, bxid, 8xVMMIDs, dataParity, 8xARTDatas] 
def constructPacketGuts(formattedHits, Bxid):
    # constants
    planesA = np.array([0, 1, 2, 3])
    planesB = np.array([4, 5, 6, 7])
    bxidCol = 0
    layerCol = 1
    planeCol = 2

    # take subset of hits at our BXID
    formattedHits = formattedHits[np.where(formattedHits[:,bxidCol] == Bxid)]
    layers = np.unique(formattedHits[:,layerCol])
    packets = []

    # iterate over layers. A layer is a 1/16 verticle subdivision of the plane,
    # but since we're currently wiring fibers in the z direction, layer is
    # always 0
    for layer in layers:
        formattedHitsInLayer = formattedHits[np.where(formattedHits[:,layerCol] == layer)]
        hitsOnA = formattedHits[np.in1d(formattedHits[:,planeCol], planesA)]
        hitsOnB = formattedHits[np.in1d(formattedHits[:,planeCol], planesB)]
        if hitsOnA.size != 0:
            packets.append([[layer, 0], buildPacketData(hitsOnA, 'A')])
        if hitsOnB.size != 0:
            packets.append([[layer, 1], buildPacketData(hitsOnB, 'B')])

    return packets
    
# buildPacketData generates the packetData used by constructPacketsGuts
# packetData takes the form:
# [numHits, bxid, 8xVMMIDs, dataParity, 8xARTDdatas]
def buildPacketData(formattedHits, side):
    # constants
    bxidCol = 0
    vmmidCol = 3
    artCol = 4
    HITS_PER_PACKET = 8

    # compute values for packetData
    [numChosen, chosenHits] = choosePreferencedHits(formattedHits, side)
    
    # extract info from formattedHits
    bffr = np.zeros([HITS_PER_PACKET - numChosen])
    bxid = np.unique(formattedHits[:, bxidCol])
    vmmid = chosenHits[:, vmmidCol]
    art = chosenHits[:, artCol]
    
    # we use buffer zeroes for null hits
    vmmIDs = np.append(vmmid, bffr)
    artData = np.append(art, bffr)

    # dataParity is an 8 bit integer with each bit representing
    # the parity of a corresponding ARTData value
    artDataParity = computeParity(artData)

    # concatenate values to build packetData
    Packet = np.hstack((numChosen, bxid, vmmIDs, artDataParity, artData))
    return Packet.tolist()

# choosePreferencedHits chooses hits for an event 
# we preferentially take hits from bottom to top and front to back
# this means a hit in the lowest VMM of each plane is taken from front to
# back, then the next lowest VMM from front to back and so on
# ie. vmm 0 on plane 0 then vmm 0 on plane 1 etc. then vmm 1 plane 0 vmm1 plane
# 1 etc.
def choosePreferencedHits(formattedHits, side):
    # constants
    planeCol = 2
    vmmIdCol = 3
    VMM_PER_LAYER = 8 # defined in extractLVA
    HITS_PER_PACKET = 8
    
    # different planes depending on side hit due to current fiber wiring
    if side == 'A':
        planes = np.array([0, 1, 2, 3])
    else:
        planes = np.array([4, 5, 6, 7])

    # get hits for appropriate side
    findhits = np.in1d(formattedHits[:,planeCol], planes)
    sideHits = formattedHits[np.in1d(formattedHits[:,planeCol], planes)]
    
    # info for choosing hits
    totalCount = sideHits[:,1].size
    numToChoose = min(HITS_PER_PACKET, totalCount)

    # initialize useful things
    numChosen = 0
    i = 0
    chosenHits = np.zeros([1,5])

    # TODO need to sort these still for front to back?
    
    # choose 8 hits
    while numChosen < numToChoose:
        poolOfHits = sideHits[np.where((sideHits[:, vmmIdCol] % VMM_PER_LAYER) == i)]
        for hit in poolOfHits:
            if numChosen != numToChoose:
                chosenHits = np.vstack((chosenHits, hit))
                numChosen += 1
            else:
                break
        i += 1

    # initialization leaves an empty first row we trim
    chosenHits = chosenHits[1:]
    return [numChosen, chosenHits]

# takes in an array of 8 artData values and computes parity for each
# returns an 8 bit number with each bit representing the parity of one value
def computeParity(x):
    # start empty so we can add parity bits as computed
    totalParity = ''

    # compute parity of every artData value and concatenate them
    for a in x:
        localParity = str(parity(int(a)))
        totalParity = ''.join((totalParity, localParity))

    # compute decimal representation of binary number
    totalParity = bin2dec(totalParity)
    
    return totalParity      

# compute parity of a number: returns 0 if even 1 if odd
def parity(x):
    binary = makebinary(x)
    digits = [int(char) for char in str(binary)]
    parity = sum(digit for digit in digits) % 2
    return parity
    
# more closely related to the matlab function
# allows a specified number of bits
# will add 0 on left til specific number of bits
def dec2bin(x, nbits):
    binary = []
    numstr = str(nbits) 
    totalstr = '0'+numstr+'b'
    for number in x:
        localbinary = format((number % 2 ** nbits), totalstr)   
        binary.append([localbinary])
    return binary

# returns binary representation of decimal number
# use: makebinary(100) NOT makebinary('100')
def makebinary(x):
    return int(bin(x)[2:])

# returns decimal representation of binary number 
# binary number doesn't have to be in string form, can be raw digits
# i.e bin2dec(110011) is correct usage, not bin2dec('0b110011')
def bin2dec(x):
    return int(str(x).encode('ascii'),2)    

# extracts the Layer, Vmmid, and Art values given plane station and strip
def extractLVA(plane, station, strip):
    # error checking
    checkError = station[np.where(station > 2)]
    if checkError.size != 0:
        print "some station value not in allowed range of [1,2]"

    # constants
    STRIPS_PER_VMM = 64
    VMMS_PER_LAYER = 8
    N_OF_LAYERS_STATION_1 = 10

    # strip number relative to wedge
    rs = strip

    # if we're on second station, add offset
    for index, item in enumerate(station):
        if item != 1:
            rs[index] = rs[index] + (VMMS_PER_LAYER * STRIPS_PER_VMM * N_OF_LAYERS_STATION_1)

    # crunch some numbers
    layer = rs/(STRIPS_PER_VMM * VMMS_PER_LAYER)
    vmmid = ((rs % (STRIPS_PER_VMM * VMMS_PER_LAYER))/STRIPS_PER_VMM) + ((plane
% 4) * VMMS_PER_LAYER)

    for index, item in enumerate(vmmid):    
        if plane[index] > 3:
            item += 32

    art = (rs % (STRIPS_PER_VMM*VMMS_PER_LAYER)) % STRIPS_PER_VMM
    
    # make sure to round down
    layer = layer.astype(int)
    vmmid = vmmid.astype(int)
    art = art.astype(int)
    return [layer, vmmid, art]
    
# parses the .txt data files to produce a summary file containg software results
# and save them in a .txt file in the data folder
# returns a numpy array of Hits in the same format as original matlab parser
# takes option inputfile and output file paths
def parseData(inputfile=input_filename, outputfile=sum_output_filename, desiredEvents=[]):

    # if we want only a chosen subset of event number hits:
    if not desiredEvents:
        selectEvents = 0
    else: 
        selectEvents = 1 
        maxDesired = max(desiredEvents)
        print 'maxDesired Event is %i' % (maxDesired)
        print 'now being selective...'

    # open file and read in all data
    lines = open(input_filename).readlines()
    print "input: %10i events and input_filename: %s" % (len(filter(lambda line:
"%Ev" in line, lines)), inputfile)

    # constants, not sure if permanent
    Hits = []
    Event_Fit_Info = []
    bcid = 20
    #bcid = 100
    bcid_delta = 5
    offsetBottom = [65, 65, 73, 73, 73, 73, 65, 65]
    offsetTop = [68, 68, 110, 110, 110, 110, 68, 68]

    # for debugging, lets just look at a subset!
    # lines = lines[0:100]

    for index, line in enumerate(lines):
        # strip the extra white space off our lines 
        line = line.strip()
    
        # find an event
        if not line.startswith("%Ev"):
            continue

        # process each line until we see event summary
        nlines = 0
        has_summary = True
        while True:
            nlines += 1
            if "%Ev" in lines[index + nlines]:
                has_summary = False
                break
            if lines[index+nlines].startswith("---"):
                # grab the footer for summary
                nlines += 1
                break

        if not(has_summary):
            continue
    
        # data for event, includes header and footer
        event_lines = lines[index : index + nlines]

        # remove more white space from lines
        event_lines = [li.strip() for li in event_lines]

        # add to the Hits array (redoing this with numpy later)
        header = event_lines[0]
        hitData = event_lines[1: -1]
        footer = event_lines[-1]
        
        # process header
        header_firstword = header.split()[0]
        event = header_firstword.replace("%Ev", "")
        event = eval(event)
        
        # here we filter for desired events
        if selectEvents:
            if event not in desiredEvents:
                # slight optimization, don't keep parsing if we have everything
                # we want already
                if event > maxDesired:
                    print "not wasting my time, you're done...."
                    break
                continue
        
        # process the hitData
        for line in hitData:
            line = ", ".join(line.split())
            time, vmm, plane, station, strip, slope = eval(line)

            # calculate offset given plane
            if station == 1:
                offset = offsetBottom[plane]
            else:
                offset = offsetTop[plane]

            # do the bcid
            hit_bcid = int(time - 50) / 25 # 60 => 0, 80 => 1, just how it is
            Hits.append([event, bcid + hit_bcid, plane, station, strip-offset, slope])

        # process the event_fit_data from footer
        footer = footer.strip("-")
        footer = footer.split("=")[-1]
        true_theta, true_phi, true_dtheta, mx, my, mxl, theta, phi, dtheta = eval(footer)
        Event_Fit_Info.append([true_theta, true_phi, true_dtheta, mx, my, mxl, theta, phi, dtheta])
        bcid += bcid_delta

    # save out the Event_Fit_Info as numpy array
    # TODO change formatting b/c they're ugly AF
    EVI = np.asarray(Event_Fit_Info)
    np.savetxt(outputfile, EVI)
        
    # return Hits array
    Hits = np.asarray(Hits)
    print np.unique(Hits[:,1]).size
    return Hits     

# inefficient parses all hits, only needs to parse one per event since event no
# is same for all of them
def findEventsInRegion(region, hitsPath='../data/stephen/testHits.npy', numEvents=''):

    if numEvents != '':
        eventsToChoose = eval(numEvents)
        print "Taking %i events in region %i" % (eventsToChoose, region)
        choosingEvents = True
    else:
        print "Taking all events in region %i..." % region
        choosingEvents = False
    
    # load the previously parsed hits (saves some time but less general)
    allHits = np.load(hitsPath)
    eventsInRegion = []
    invalidEvents=[]
    STRIPS_IN_REGION = 512
    
    # iterate looking for hits that match specs
    for hit in allHits:
        strip = int(hit[4]) 
        eventNo = hit[0]
        if (strip >= region*512) & (strip < (region+1)*512):
            eventsInRegion.append(eventNo)
        else:
            continue

    # stupid way of getting unique event numbers
    eventList = list(set(eventsInRegion))
    eventList.sort()

    # need to remove events with members outside of region
    for event in eventList:
        eventMembers = allHits[np.where(allHits[:,0] == event)] 
        for item in eventMembers:
            strip = int(item[4])
            if not ((strip >= region*512) & (strip < (region+1)*512)):
                invalidEvents.append(event)
                break
    for invalid in invalidEvents:
        eventList.remove(invalid)
    if choosingEvents:
        eventList = eventList[0:eventsToChoose]

    print "Events found in region: %i" % len(eventList)

    return eventList
