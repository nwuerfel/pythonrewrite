import numpy as np
import parseAndProcess as pap


def screenPackets():

	gbtPackets = np.load('gbtPacketBin.npy')
	screenedPackets = []

	badHitMap = '00000011101111000000000000001000'

	for packet in gbtPackets:
		hitmap = packet[56:88]
		maskedMap = int(badHitMap,2) & int(hitmap,2)
		if maskedMap == 0:
			screenedPackets.append(packet)

	packetHex = pap.binStr2hexStr(screenedPackets)
	goodPackets = []	
	for word in packetHex:
		print word
		goodPackets.append(word+'\n')

	pap.writeOut('screenedPackets.txt', goodPackets)
		

